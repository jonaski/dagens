﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.NodeServices;

namespace Dagens.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly INodeServices _nodeServices;

        public HomeController(INodeServices nodeServices)
        {
            _nodeServices = nodeServices;
        }

        public async Task<IActionResult> Index()
        {
            string message = await _nodeServices.InvokeAsync<string>("./Scripts/Scrapers/danilo");
            return View(message);
        }
    }
}