﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dagens.Web.Models
{
    public class Scraper
    {
        public virtual Guid Id { get; set; }
        public virtual string ScriptPath { get; set; }
    }
}
