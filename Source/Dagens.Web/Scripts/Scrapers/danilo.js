﻿var request = require('request');
var cheerio = require('cheerio');

module.exports = function (callback)
{
    var url = 'http://www.danilorestaurang.se/bergakungen/lunchmeny/';

    request(url, function (error, response, html) {
        if (!error) {
            var $ = cheerio.load(html);
            var result = "";

            $("article.page div.columns div.row").each(function ()
            {
                result += $("h3", this).text();
            });
        }

        callback(null, result);
    })
}