﻿using System.Threading.Tasks;

namespace Dagens.Web.Services
{
    public interface IFileService
    {
        Task<string> ReadFile(string path);
        void WriteFile(string path, string contents);
    }
}