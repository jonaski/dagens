﻿using Dagens.Web.Models;
using Microsoft.AspNetCore.NodeServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dagens.Web.Services
{
    public class ScraperService
    {
        private readonly INodeServices _nodeServices;

        public ScraperService(INodeServices nodeServices)
        {
            _nodeServices = nodeServices;
        }

        public virtual async Task<IEnumerable<ScraperResult>> Execute(Scraper scraper)
        {
            string result = await _nodeServices.InvokeAsync<string>(scraper.ScriptPath);

            throw new NotImplementedException();
        }
    }
}
