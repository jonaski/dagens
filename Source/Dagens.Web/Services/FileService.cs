﻿using System.IO;
using System.Threading.Tasks;

namespace Dagens.Web.Services
{
    public class FileService : IFileService
    {
        public virtual async Task<string> ReadFile(string path)
        {
            return await File.ReadAllTextAsync(path);
        }

        public virtual async void WriteFile(string path, string contents)
        {
            await File.WriteAllTextAsync(path, contents);
        }
    }
}
