﻿using Dagens.Web.Models;
using Dagens.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dagens.Web.Repositories
{
    public class ScraperRepository
    {
        private readonly IFileService _fileService;

        public ScraperRepository(IFileService fileService)
        {
            _fileService = fileService;
        }

        public virtual async Task<Scraper> Add(Guid id, string script)
        {
            throw new NotImplementedException();
        }

        public virtual void Update(Scraper scraper)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<Scraper> Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<IEnumerable<Scraper>> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
